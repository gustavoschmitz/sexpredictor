## Sex predictor

This program was developed in a recruitment process context.

### How to use:
1 - Install the requirements running 'pip install -r requirements.txt'. Obs.: Although the flag '-r' is not mentioned on the file describing the challenge, it is necessary here.

2 - Make sure that there is a file named 'model.bin' inside the program's folder.

3 - Run the script passing the sample's path, 'e.g. python sex_predictor.py newsample.csv'. Obs.: If your sample file is not on the same folder as the script, you should pass the full file's path.

### Output:
The program output is a new csv file containing the sex predictions that will be saved on the program's path.