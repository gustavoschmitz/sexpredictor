import csv

import pandas
import numpy as np

import pickle

import sys
from sklearn.linear_model import LinearRegression


class DataAdjuster:
    @classmethod
    def adjust_nan(cls, data_set_with_nan: pandas.DataFrame) -> pandas.DataFrame:
        """
        This method replaces the NaN cells with the mean value of the column.

        :param data_set_with_nan: Data containing the NaN cells.

        :return: Data without NaN cells.
        """
        adjusted_data = data_set_with_nan.copy()
        for key, values in adjusted_data.iteritems():
            if np.isnan(values).any():
                mean = np.nanmean(values)
                values.replace([np.nan], [mean], inplace=True)
        return adjusted_data

    @classmethod
    def adjust_columns(cls, data_set_with_useless_columns: pandas.DataFrame, training_data: bool=False) -> pandas.DataFrame:
        """
        This method deletes the useless columns and set 'sex' as the last column in a training data execution.

        :param data_set_with_useless_columns: Data with original columns.

        :param training_data: Flag that indicates if it is a training data execution.

        :return: Data without useless columns.
        """
        adjusted_data = data_set_with_useless_columns.drop(columns=['cp', 'nar', 'sk', 'hc', 'trf'])
        if training_data:
            adjusted_data = adjusted_data[[c for c in adjusted_data if c not in ['sex']] + ['sex']]
        return adjusted_data

    @classmethod
    def replace_qualitative_sex_values(cls, data_set_with_qualitative_values: pandas.DataFrame, male_value: int = 0,
                                       female_value: int = 100) -> pandas.DataFrame:
        """
        This method replaces the qualitative sex values ('F', 'M') for quantitative values.

        :param data_set_with_qualitative_values: Data with qualitative values.

        :param male_value: Quantitative male value.

        :param female_value: Quantitative female value.

        :return: Data with the quantitative sex values.
        """
        adjusted_data = data_set_with_qualitative_values.replace(['M', 'm', 'F', 'f'], [male_value, male_value,
                                                                                        female_value, female_value])
        return adjusted_data

class ModelHandler:
    def __init__(self):
        self.model = None
        self.load_model()

    @classmethod
    def create_model(cls):
        """
        This method is used to create the predictor model.

        :return: Predictor model.
        """
        _data_set = pandas.read_csv('test_data_Gustavo_Albino.csv', index_col=0)
        transformed_data = DataAdjuster.adjust_columns(_data_set, training_data=True)
        transformed_data = DataAdjuster.replace_qualitative_sex_values(transformed_data)
        transformed_data = DataAdjuster.adjust_nan(transformed_data)

        features = transformed_data.iloc[:, :-1]
        result = transformed_data.iloc[:, -1]

        reg = LinearRegression()
        model = reg.fit(features, result)
        print(model.score(features, result))
        pickle.dump(model, open(sys.argv[0] + 'model.bin', 'wb'))

    def load_model(self):
        """
        This method is used to load the predictor model from a binary file into the program.

        :return: Predictor model.
        """
        self.model = pickle.load(open(sys.argv[0].split('sex_predictor.py')[0] + 'model.bin', 'rb'))

class SexPredictor:
    @staticmethod
    def predict_sex(model, transformed_data) -> list:
        """
        This method is used to predict the sex column.

        :param model: Predictor model.

        :param transformed_data: Data already adjusted.

        :return: List of sex values, e.g. ['F', 'F', 'M', 'M', 'F']
        """
        prediction = model.predict(transformed_data)
        m_or_f = []
        for item in prediction:
            if item > 50:
                m_or_f.append('F')
            else:
                m_or_f.append('M')
        return m_or_f

    @staticmethod
    def write_csv_answer(sex_list: list):
        """
        This method writes the sex predictions on a csv file.

        :param sex_list: List containing the sex predictions.
        """
        file = open('newsample_PREDICTIONS_Gustavo_Schmitz_Albino.csv', 'w', newline='')

        with file:
            sex_list = [["sex"]] + sex_list
            write = csv.writer(file)
            write.writerows(sex_list)
            file.close()

if __name__ == '__main__':
    model_handler = ModelHandler()
    trained_model = model_handler.model
    data_set = pandas.read_csv(sys.argv[-1], index_col=0)
    data_set = DataAdjuster.adjust_columns(data_set)
    data_set = DataAdjuster.adjust_nan(data_set)
    predictor = SexPredictor()
    sex_predictions = predictor.predict_sex(trained_model, data_set)
    predictor.write_csv_answer(sex_predictions)
